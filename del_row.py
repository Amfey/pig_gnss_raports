def del_row(inp = 'chelm_141205.csv', out = 'outfile.csv'):
    

# Kasuje dwie pierwsze linie z pliku


    FIRST_ROW_NUM = 1  # or 0
    ROWS_TO_DELETE = {1, 2}

    with open(inp, 'rt') as infile, open(out, 'wt') as outfile:
        outfile.writelines(row for row_num, row in enumerate(infile, FIRST_ROW_NUM)
                            if row_num not in ROWS_TO_DELETE)